VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#2.0#0"; "Mscomctl.ocx"
Begin VB.UserControl DBTree 
   ClientHeight    =   4185
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5055
   ScaleHeight     =   4185
   ScaleWidth      =   5055
   Begin ComctlLib.ListView lv 
      Height          =   3975
      Left            =   2640
      TabIndex        =   2
      Top             =   120
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   7011
      View            =   2
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Categor�a"
         Object.Width           =   2540
      EndProperty
   End
   Begin ComctlLib.TreeView tv 
      Height          =   3975
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   7011
      _Version        =   393217
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      Caption         =   "Label1"
      Height          =   195
      Left            =   4320
      TabIndex        =   1
      Top             =   3480
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "DBTree"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public Event NodoSeleccionado(codigo$, cpadre$, nombre$)
Private rq(1 To 10) As New rdoQuery
Private cSrc$, cDst$
Private Dragging As Boolean

'El programa principal debe llamar a esta funci�n con rdoConnection
'ya establecida. Par�metros:
'tn: Nombre de la tabla que tiene los nodos (codigo,descripcion,tipo)
'c,d,t: Nombres de los campos de la tabla anterior (codigo,descripcion,tipo)
'tr: Nombre de la tabla que tiene las ramas del arbol (cpadre,chijo)
'p,h: Nombre de los campos de la tabla anterior que contienen los codigos (padre,hijo)
Public Sub Inicializa(rdocaux As rdoConnection, tn$, c$, d$, t$, tr$, p$, h$)
    Call InitRQ(rdocaux, tn, c, d, t, tr, p, h)
    Call InitTV
    Call InitLV
End Sub

'Inicializacion de los rdoQuery
Private Sub InitRQ(rdocaux As rdoConnection, tn$, c$, d$, t$, tr$, p$, h$)
    Dim i%
    'Select de inicializacion (aquellos que no tienen padre)
    rq(1).SQL = "Select " & c & "," & d & "," & t & _
        " From " & tn & " Where " & c & " in (Select " & _
        h & " From " & tr & " Where " & p & "=0)"
    'Select para recoger los hijos
    rq(2).SQL = "Select " & c & "," & d & "," & t & _
        " From " & tn & " Where " & c & " in (Select " & _
        h & " From " & tr & " Where " & p & "=?)"
    'Cambia el nombre de un nodo
    rq(3).SQL = "Update " & tn & " set " & d & "=? where " & c & "=?"
    'Crea nuevo hijo
    rq(4).SQL = "Insert into " & tr & "(" & p & "," & h & ") values (?,?)"
    'Comprobaci�n de Referencias c�clica
    rq(5).SQL = "Select count(*) from dual where ? in " & _
        "(Select distinct " & h & " from " & tr & " start with " & p & "=? " & _
        "Connect by prior " & h & "=" & p & ")"
    'Borrar un hijo
    rq(6).SQL = "Delete from " & tr & " where " & p & "=? and " & h & "=?"
    'Todas las caegor�as para el ListView
    rq(7).SQL = "Select " & c & "," & d & " from " & tn
    'Crear nueva categor�a
    rq(8).SQL = "Insert into " & tn & "(" & c & "," & d & ") values (?,?)"
    'Siguiente c�digo de categor�a
    rq(9).SQL = "Select 1+max(" & c & ") from " & tn
    'Borrar una categor�a
    rq(10).SQL = "Begin delete from " & tr & " where " & h & "=?; " & _
        "Delete from " & tr & " where (" & p & "," & h & ") in (Select " & _
        p & "," & h & " from " & tr & " start with " & p & "=? connect by " & _
        "prior " & h & "=" & p & "); delete from " & tn & " where " & c & "=?;" & _
        " end;"
    For i = 1 To UBound(rq)
        Set rq(i).ActiveConnection = rdocaux
        rq(i).Prepared = True
    Next
End Sub

Private Sub InitTV()
    Dim rs As rdoResultset, n As Node
    tv.Nodes.Clear
    Set n = tv.Nodes.Add(, , "0:", "Raiz")
    n.Tag = "0"
    Set rs = rq(1).OpenResultset
    While Not rs.EOF
        Set n = tv.Nodes.Add(1, tvwChild, "0:" & rs(0) & ".", rs(1))
        n.Tag = rs(0)
        tv.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
End Sub

Private Sub InitLV()
    Dim rs As rdoResultset
    Set rs = rq(7).OpenResultset
    While Not rs.EOF
        lv.ListItems.Add , "C" & rs(0), rs(1)
        rs.MoveNext
    Wend
End Sub

Private Sub DeleteCat(c%)
    Dim i%
    If c <> 0 Then
        rq(10).rdoParameters(0).Value = c
        rq(10).rdoParameters(1).Value = c
        rq(10).rdoParameters(2).Value = c
        rq(10).Execute
        i = 1
        While i <= tv.Nodes.Count
            If tv.Nodes.Item(i).Tag = CStr(c) Then
                tv.Nodes.Remove tv.Nodes.Item(i).Key
            Else
                i = i + 1
            End If
        Wend
        lv.ListItems.Remove "C" & c
    Else
        MsgBox "La categor�a especial 'Raiz' no se puede borrar (codigo 0)"
    End If
End Sub

Private Sub DeleteCatHija(n As Node)
    If n.Tag <> 0 Then
        rq(6).rdoParameters(0) = n.Parent.Tag
        rq(6).rdoParameters(1) = n.Tag
        rq(6).Execute
        tv.Nodes.Remove n.Key
    Else
        MsgBox "La categor�a especial 'Raiz' no se puede borrar (codigo 0)"
    End If
End Sub

Private Sub AppendChild(padre$, padreKey$, hijo$, hijoText$)
    Dim n As Node, rs As rdoResultset
    rq(5).rdoParameters(0) = padre
    rq(5).rdoParameters(1) = hijo
    On Error GoTo RefCircular
    Set rs = rq(5).OpenResultset
    If rs(0) = 0 Then
        rq(4).rdoParameters(0).Value = padre
        rq(4).rdoParameters(1).Value = hijo
        rq(4).Execute
        On Error GoTo IsThing
        Set n = tv.Nodes.Item(padreKey & "dummy")
    Else
RefCircular:
        On Error GoTo 0
        MsgBox "Referencia circular! No se puede a�adir como hijo"
    End If
    Exit Sub
IsThing:
    On Error GoTo 0
    Set n = tv.Nodes.Add(padreKey, tvwChild, _
        padreKey & hijo & ".", _
        hijoText)
    n.Tag = hijo
    tv.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
End Sub

Private Function ChangeName(n As Node, newname$) As Boolean
    Dim i%
    On Error GoTo NoEdit
    rq(3).rdoParameters(0).Value = newname
    rq(3).rdoParameters(1).Value = n.Tag
    rq(3).Execute
    For i = 1 To tv.Nodes.Count
        If tv.Nodes.Item(i).Tag = tv.SelectedItem.Tag Then
            tv.Nodes.Item(i).Text = newname
        End If
    Next
    ChangeName = True
    Exit Function
NoEdit:
    ChangeName = False
End Function

Private Sub CreateCat(nc$)
    Dim rs As rdoResultset
    Set rs = rq(9).OpenResultset
    rq(8).rdoParameters(0).Value = rs(0)
    rq(8).rdoParameters(1).Value = nc
    rq(8).Execute
    lv.ListItems.Add , "C" & rs(0), nc
End Sub

Private Sub lv_KeyUp(KeyCode As Integer, Shift As Integer)
    Dim str$
    Select Case KeyCode
    Case 45:
        str = InputBox("Nombre de la categor�a:", "Creando categor�a...")
        Call CreateCat(str)
    Case 46:
        Call DeleteCat(Right(lv.SelectedItem.Key, Len(lv.SelectedItem.Key) - 1))
    End Select
End Sub

Private Sub tv_Expand(ByVal Node As ComctlLib.Node)
    Dim n As Node, rs As rdoResultset, str$
    On Error Resume Next
    Err.Clear
    Set n = tv.Nodes.Item(Node.Key & "dummy")
    str = n.Key
    On Error GoTo 0
    If str <> "" Then
        tv.Nodes.Remove n.Key
        rq(2).rdoParameters(0).Value = Node.Tag
        Set rs = rq(2).OpenResultset
        While Not rs.EOF
            Set n = tv.Nodes.Add(Node, tvwChild, Node.Key & rs(0) & ".", rs(1))
            n.Tag = rs(0)
            tv.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
            rs.MoveNext
        Wend
        rs.Close
    End If
End Sub

Private Sub tv_KeyUp(KeyCode As Integer, Shift As Integer)
    Dim i%, kmat$
    If KeyCode = 46 Then 'tecla suprimir
        Call DeleteCatHija(tv.SelectedItem)
    End If
End Sub

Private Sub tv_NodeClick(ByVal Node As ComctlLib.Node)
    lbl.Drag 0
    Dragging = False
    If Node.Key <> "0:" Then
        RaiseEvent NodoSeleccionado( _
            Node.Tag, _
            Node.Parent.Tag, _
            Node.Text)
    End If
End Sub

Private Sub tv_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo IsNothing
    cSrc = tv.HitTest(x, y).Tag
    Dragging = True
    lbl.Top = y
    lbl.Left = x
    Exit Sub
IsNothing:
    Dragging = False
    lbl.Drag 0
End Sub

Private Sub lv_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo IsNothing
    cSrc = Right(lv.HitTest(x, y).Key, Len(lv.HitTest(x, y).Key) - 1)
    Dragging = True
    lbl.Top = y
    lbl.Left = tv.Width + x
    Exit Sub
IsNothing:
    Dragging = False
    lbl.Drag 0
End Sub

Private Sub tv_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Dragging Then
        lbl.Drag 1
    End If
End Sub

Private Sub lv_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Dragging Then
        lbl.Drag 1
    End If
End Sub

Private Sub tv_DragDrop(Source As Control, x As Single, y As Single)
    If Dragging Then
        On Error GoTo IsNothing
        cDst = tv.HitTest(x, y).Tag
        If cDst = cSrc Then GoTo IsNothing
        Call AppendChild(cDst, tv.HitTest(x, y).Key, cSrc, lv.ListItems("C" & cSrc))
IsNothing:
        Dragging = False
        lbl.Drag 0
    End If
    Screen.MousePointer = 0
End Sub

Private Sub tv_AfterLabelEdit(Cancel As Integer, NewString As String)
    Cancel = ChangeName(tv.SelectedItem, NewString)
End Sub

'El TreeView Ocupa todo el control
Private Sub UserControl_Resize()
    
    tv.Left = 0
    tv.Top = 0
    tv.Height = UserControl.Height
    tv.Width = UserControl.Width / 2
    
    lv.Left = UserControl.Width / 2
    lv.Top = 0
    lv.Height = UserControl.Height
    lv.Width = UserControl.Width / 2
   
End Sub

